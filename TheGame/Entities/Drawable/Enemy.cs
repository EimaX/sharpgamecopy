﻿using System;
using System.Text;
using System.Threading;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheGame.Entities.Animation;
using TheGame.Entities.Logical;

namespace TheGame.Entities.Drawable
{
    internal class Enemy : Entity
    {
        private enum Directions
        {
            up,
            down,
            left,
            right
        }
        private Directions _direction;
        private readonly SpriteBatch _spriteBatch;
        private const int force = 5;
        public Body Body { get; set; }
        private Texture2D _sprite;
        EnemyAnimation frame = new EnemyAnimation();
        private bool ChangeFrame = true;
        private bool judejimas = true;
        private Player playeris;
        private Texture2D _end;
        private float speed;
        private readonly Random random = new Random();
        private int cher;
        private bool dead = false;
        private int deathtime;
        public bool endgame = false;

        public Enemy(Game game, World world, Vector2 position,Player player, int spawntime) : base(game)
        {
            playeris = player;
            _spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            //TODO: create body from texture
            Body = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(20f), ConvertUnits.ToSimUnits(20f), 1f);
            Body.BodyType = BodyType.Dynamic;
            Body.Mass = 25;
            Position = position;
            Body.Position = ConvertUnits.ToSimUnits(Position);
            judeti(player);
            Thread.Sleep(10);
            speed = (float)(random.NextDouble() *  0.3 + 0.7); 
            cher = random.Next(3);
            deathtime = spawntime + 6;

        }

        public override void Initialize()
        {
            //InputHandler.Instance[ActionControlls.Right].OnDown += OnActionDown;
            base.Initialize();
        }

        //private void OnActionDown(GameTime gameTime, ActionControlls action)
        //{
        //    var force = Vector2.Zero;
         
        //     judeti(playeris);
                 

            
        //    ChangeFrame = true;
        //    Body.ApplyLinearImpulse(force, Body.Position);
        //}

        private void judeti(Player player)
        {
            var force = Vector2.Zero;
            var forceY = Vector2.Zero;
            if (Body.Position.X < playeris.Body.Position.X)
                force = new Vector2(speed, 0f); 
            else
                force = new Vector2(speed*-1, 0f);
            if (Body.Position.Y > playeris.Body.Position.Y+1)
                forceY = new Vector2(0f, -5f);
            Body.ApplyLinearImpulse(force, Body.Position);
            Body.ApplyLinearImpulse(forceY, Body.Position);

            

        }





        private bool Collided()
        {
            if (Body.Position.X > playeris.Body.Position.X - 0.5 && Body.Position.X < playeris.Body.Position.X + 0.48
                && Body.Position.Y > playeris.Body.Position.Y && Body.Position.Y < playeris.Body.Position.Y + 0.6)
            {
                if (cher == 0)
                {
                    dead = true;
                    Body.Position = new Vector2(0, 20000);
                    playeris.lives += 10;
                    if (!playeris.endgame)
                        Console.WriteLine("Mokymosi motyvacija: " + playeris.lives);
                }
                else
                {
                    dead = true;
                    Body.Position = new Vector2(0,20000);
                    playeris.lives -= 20;
                    if (!playeris.endgame)
                        Console.WriteLine("Mokymosi motyvacija: " + playeris.lives);
                    if (playeris.lives < 0)
                    {
                        if (!playeris.endgame)
                        {
                            var ms = playeris.time%1000;
                            var s = (int) playeris.time/1000;
                            Console.WriteLine("Nebera motyvacijos :(  Universitete isbuvote: " + s + "," + ms + " sekundziu");
                            Console.WriteLine("tete isbuvote: " + playeris.time);
                        }
                        Camera2D.Instance.Focus = null;
                        Camera2D.Instance.Position = new Vector2(0, 0);
                        GraphicsDevice.Clear(Color.Black);
                        playeris.endgame = true;
                        
                    }
                }
                
                return true;
            }
            return false;
        }
     

        protected override void LoadContent()
        {

            //_sprite = Game.Content.Load<Texture2D>("Textures/Entities/Bite/bitės.0000.png");
            //Console.WriteLine(frame.GetFrame(ChangeFrame, cher));

            _sprite = Game.Content.Load<Texture2D>(frame.GetFrame(ChangeFrame, cher));
            _end = Game.Content.Load<Texture2D>("Textures/end.png");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (!dead)
            {
                Position = ConvertUnits.ToDisplayUnits(Body.Position);
                _sprite = Game.Content.Load<Texture2D>(frame.GetFrame(ChangeFrame, cher));

                judeti(playeris);
                
                base.Update(gameTime);
            }
            if (deathtime < gameTime.TotalGameTime.Seconds)
            {
                Body.ApplyLinearImpulse(new Vector2(0f, -50f), Body.Position);
                
            }
            if (deathtime + 1 < gameTime.TotalGameTime.Seconds)
            {
                Body.ApplyLinearImpulse(new Vector2(1000f, -50f), Body.Position);
                dead = true;
                Body.Position = new Vector2(0, 20000);
            }
           

        }

        public override void Draw(GameTime gameTime)
        {
            if (!dead)
            {
                _spriteBatch.Begin(Camera2D.Instance);
                Update(gameTime);
                _spriteBatch.Draw(_sprite, new Rectangle((int) Position.X, (int) Position.Y, 64, 64), _sprite.Bounds,
                    Color.White, 0f, new Vector2(1000, 1000),
                    _direction == Directions.left ? SpriteEffects.FlipHorizontally : SpriteEffects.None, 0);
                //_spriteBatch.Draw(_sprite, Position, null, Color.White, Body.Rotation, new Vector2(1000,1000), 0.01f, SpriteEffects.None, 0f);

                if (Collided())
                    _spriteBatch.Draw(_end, Position, null, Color.White, Body.Rotation, new Vector2(1000, 1000), 0.02f,
                        SpriteEffects.None, 0f);
                _spriteBatch.End();

                base.Draw(gameTime);
            }
        }
    }
}
