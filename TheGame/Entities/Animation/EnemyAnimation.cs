﻿using System;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheGame.Entities.Logical;

namespace TheGame.Entities.Animation
{
    class EnemyAnimation
    {

        const int AnimationSpeed = 5; // animacijos greitis
        private int i = 0;
        private int j = 0;
        Random rand = new Random();


        public string GetFrame(bool change, int cher)
        {
            switch (cher)
            {
                case 1:
                    if (change)
                    {
                        j++;
                        if (j > rand.NextDouble()*9 + 1)
                        {
                            i++;
                            j = 0;
                        }
                        if (i > 11) i = 1;
                        if (i > 9) return "Textures/Entities/Kompas/kompas.00" + i + ".png";
                        else return "Textures/Entities/Kompas/kompas.000" + i + ".png";
                    }
                    return "Textures/Entities/Kompas/kompas.0001.png";

                case 2:
                    if (change)
                    {
                        j++;
                        if (j > rand.NextDouble() * 14 + 1)
                        {
                            i++;
                            j = 0;
                        }
                        if (i > 11) i = 1;
                        if (i > 9) return "Textures/Entities/Testas/testas00" + i + ".png";
                        else return "Textures/Entities/Testas/testas000" + i + ".png";
                    }
                    return "Textures/Entities/Kompas/testas0001.png";

                case 3:
                    if (change)
                    {
                        j++;
                        if (j > rand.NextDouble() * 9 + 1)
                        {
                            i++;
                            j = 0;
                        }
                        if (i > 11) i = 1;
                        if (i > 9) return "Textures/Entities/Knyga/knyga00" + i + ".png";
                        else return "Textures/Entities/Testas/knyga000" + i + ".png";
                    }
                    return "Textures/Entities/Kompas/knyga0001.png";



            }

            if (change)
            {
                j++;
                if (j > rand.NextDouble() * 9 + 1)
                {
                    i++;
                    j = 0;
                }
                if (i > 11) i = 1;
                if (i > 9) return "Textures/Entities/5/penki00" + i + ".png";
                else return "Textures/Entities/5/penki000" + i + ".png";
            }
            return "Textures/Entities/Kompas/5/penki0001.png";


        }



    }
}
